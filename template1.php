<div class="container-fluid">
    <div class="row" style="margin-left: -30px;">
        <div class="col col-xs-12 col-sm-7 col-md-8 col-lg-9"></div>
        <div class="col col-xs-12 col-sm-5 col-md-4 col-lg-3">
            <div id="btn_flotante_umbot" class="float_barra" onclick="mostrar_chatbot()">
                <div class="panel-heading" style="background: #009AD9; color: #fff; cursor: pointer; ">
                    Poder Judicial Mendoza
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-window-maximize"></i>
                        </button>
                    </div>
                </div>
                <div style="height: 0px;">
                    <div class="panel-footer" style="background-color: #dbdbdb; border: 1px solid #d6d6d6;">
                        <div class="input-group">
                            <input disabled type="text" class="form-control input-sm" placeholder="Escriba su mensaje aqu&iacute;..." maxlength="100" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm" style="padding: 5px 10px; background: #1B60AB; color: #fff">
                                    <i class="fa fa-send"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-left: -30px;">
        <div class="col col-xs-12 col-sm-7 col-md-8 col-lg-9"></div>
        <div class="col col-xs-12 col-sm-5 col-md-4 col-lg-3">
            <div id="contenedor_umbot" class="float_chatbot" style="display: none;">
                <div class="panel-heading" style="cursor: pointer; background: #009AD9; color: #fff;" onclick="ocultar_chatbot()">
                    Poder Judicial Mendoza
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" onclick="ocultar_chatbot()">
                            <i class="fa fa-window-minimize"></i>
                        </button>
                    </div>
                </div>
                <div id="contenedor_chat" style="height: 0px;">
                    <div class="panel-body-bot" style="background: #fcfcfc; border: 1px solid #DADADA;" id="div-chat">
                        <ul class="chat" id="chat-list"></ul>
                    </div>
                    <div class="panel-footer" style="background-color: #dbdbdb; border: 1px solid #d6d6d6;">
                        <div class="input-group">
                            <input id="m" disabled type="text" class="form-control input-sm" placeholder="Escriba su mensaje aqu&iacute;..." maxlength="100" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm" id="btn-chat" style="padding: 5px 10px; background: #1B60AB; color: #fff" onclick="enviarMensaje()">
                                    <i class="fa fa-send"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>