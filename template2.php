<div id="btn_flotante_umbot" class="float_barra" onclick="mostrar_chatbot()">
		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-8 col-lg-9"></div>
			<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
				<div class="panel-heading bg-info" style="color: #fff; cursor: pointer;">
					<img class="animate infinite pulse" src="<?php echo $options['chatbot_um_logo_url']; ?>" height="40px" /> <?php echo $options['chatbot_um_titulo_ventana'] ?>
					<div class="btn-group pull-right">
						<div class="btn-group pull-right">
					<button type="button" class="btn btn-default btn-md" data-toggle="dropdown">
						<i class="fa fa-chevron-up"></i>
					</button>
				</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="contenedor_umbot" class="float_chatbot os-android" style="display: none;z-index: 9999">
		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-8 col-lg-9"></div>
			<div class="col-xs-12 col-sm-5 col-md-5 col-lg-3">
				<div class="panel-heading bg-info" id="headerbar" style="cursor: pointer;color: #fff;" onclick="ocultar_chatbot()">
					<img class="animate infinite pulse" src="<?php echo $options['chatbot_um_logo_url']; ?>" height="40px" /> <?php echo $options['chatbot_um_titulo_ventana'] ?>
					<div class="btn-group pull-right">
						<div class="btn-group pull-right">
					<button type="button" class="btn btn-default btn-md" data-toggle="dropdown">
						<i class="fa fa-chevron-down"></i>
					</button>
				</div>
					</div>
				</div>
				<div id="contenedor_chat" style="height: 0px;">
					<div class="panel-body" style="background: #fcfcfc; border: 1px solid #DADADA;" id="div-chat">
						<ul class="chat" id="chat-list"></ul>
					</div>
					<div class="panel-footer" id="messagebar" style="background-color: #dbdbdb; border: 1px solid #d6d6d6;">
						<div class="input-group">
							<input  id="m" disabled type="text" class="form-control input-sm" placeholder="Escriba su mensaje aqu&iacute;..." maxlength="100" />
							<span class="input-group-btn">
								<button class="btn btn-sm" id="btn-chat" style="padding: 5px 10px; background: #00AE97; color: #fff" onclick="enviar()">
									<i class="fa fa-paper-plane"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>