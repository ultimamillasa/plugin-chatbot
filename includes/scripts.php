<?php $url = $_GET['url'] ?>
<?php print_r($url) ?>
<script type="text/javascript">
function mostrar_chatbot() {
    jQuery("#btn_flotante_umbot").hide();
    jQuery("#contenedor_umbot").show();
    jQuery("#contenedor_chat").animate({height:"422px"}, 250, function() {
        jQuery("#m").focus();
    });
}

function ocultar_chatbot() {
    jQuery("#contenedor_chat").animate({height:"0px"}, 250, function() {
        jQuery("#contenedor_umbot").hide();
        jQuery("#btn_flotante_umbot").show();
    });
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function fecha() {
    var date = new Date();
    var hora = addZero(date.getHours());
    var minuto = addZero(date.getMinutes());

    return hora + ":" + minuto;
}

function enviar(mensaje = "", control = false, ocultar = false) {
    var mostrar = false;
    if (control == true) {
        var texto = jQuery.trim(mensaje);
    } else {
        var texto = jQuery.trim(jQuery("#m").val());
    }
    if (texto != "") {
        jQuery("#m").val("");
        if (ocultar == false) {
            mostrar_mensaje("chat-list", texto, 1, 0, true);
        }
        //console.log(texto);
        texto = texto.replace(/[0-9`~¡!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,'');
        //console.log(texto);

        mostrar_mensaje("chat-list", "<div align='left' style='height: 20px;'><img src='http://www.guaymallen.gob.ar/wp-content/uploads/2019/04/escribiendo.gif' width = '20px;'  /></div>", 0, 1, true);

        jQuery.ajax({
            method: "POST",
            url: "<?php echo $url ?>",
            dataType: 'json',
            async: true,
            data: {
                "q": texto,
            },
            success: function(array_respuestas) {
                jQuery.each(array_respuestas, function(indice_respuesta, respuesta) {
                    if (respuesta.text.substring(0, 10) == "DEPURACION") {
                        mostrar = false;
                    } else {
                        mostrar = true;
                    }
                    if (mostrar == true) {
                        var leyenda;
                        if (!jQuery.isEmptyObject(respuesta)) {
                            leyenda = respuesta.text;
                        } else {
                            leyenda = "Lo lamento, no he entendido lo que has escrito. Intenta ser un poco m&aacute;s espec&iacute;fico, por favor.";
                            console.log("Error en la respuesta del bot. Salvado por jQuery"); // BORRAR
                        }
                        setTimeout(
                            function() {
                                jQuery("#mensaje_temporal").remove();
                                
                                var botones = "";
                                if(respuesta.buttons !== undefined){
                                    if(respuesta.buttons.length > 0){
                                        var btn = "";
                                        jQuery.each(respuesta.buttons, function(auxiliar, boton) {
                                            
                                            var valores = JSON.parse(boton.payload);
                                            valores = valores.valores[0];
                                            var tipo = valores.tipo;

                                            switch (parseInt(tipo)) {
                                                case 0: // TEXTO
                                                    btn = '<a onclick="enviar(' + "'" + jQuery.trim(valores.texto) + "'" + ',true)" class="btn_rasa sombra_leve btn" style="background: #684689; color: #fff; cursor: pointer">' + boton.title + '</a>';
                                                    break;
                                                case 1: // LINK
                                                    btn = '<a href="' + valores.url + '" target="_blank" class="btn_rasa sombra_leve btn" style="background: #684689; color: #fff">' + boton.title + ' <i class="fa fa-share"></i></a>';
                                                    break;
                                                case 2: // CORREO
                                                    btn = '<a mailto="' + valores.url + '" class="btn_rasa sombra_leve btn" style="background: #684689; color: #fff; cursor: pointer">' + boton.title + '</a>';
                                                    break;
                                                default: // DEFAULT
                                                    break;
                                            }
                                            botones += btn;
                                        });
                                        botones = "<br />" + botones;
                                    }
                                }

                                mostrar_mensaje("chat-list", leyenda + botones, 0, 0);
                            }, 1000);
                    }
                });
            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        });
    }
}

function mostrar_mensaje(id, mensaje, tipo, temporal, ocultar = false) {
    jQuery("#m").prop('disabled', true);
    var posicion = "left";
    var avatar = "favicon.png";
    var nombre = "Chatbot";
    var clase = "him";
    if (tipo == 1) {
        posicion = "right";
        avatar = "mujer.jpg";
        if (localStorage.getItem('aleatorio_avatar') == 1) {
            avatar = "hombre.jpg";
        }
        nombre = "Vecino/a";
        clase = "me";
    }

    var contenido = "";
    if (temporal == 1) {
        contenido += '<li class="' + posicion + ' clearfix" id="mensaje_temporal">';
    } else {
        contenido += '<li class="' + posicion + ' clearfix">';
    }

        contenido += '<div class="chat-body clearfix">';
            contenido += '<div class="header">';
                switch (posicion) {
                    case "right":
                        contenido += '<small class="text-muted">';
                            contenido += '<span class="glyphicon glyphicon-time"></span>' + fecha();
                        contenido += '</small>';
                        contenido += '<strong class="pull-' + posicion + ' primary-font">' + nombre + '</strong>';
                        break;
                    case "left":
                        contenido += '<strong class="primary-font">' + nombre + '</strong>';    
                        contenido += '<small class="pull-right text-muted">';
                            contenido += '<span class="glyphicon glyphicon-time"></span>' + fecha();
                        contenido += '</small>';
                        break;
                }
            contenido += '</div>';
            contenido += '<p>';
                contenido += '<div class="sombra ' + clase + '">';
                    contenido += mensaje;
                contenido += '</div>';
            contenido += '</p>';
        contenido += '</div>';
            
    contenido += '</li>';
    jQuery("#" + id).append(contenido);
    
    setTimeout(
        function() {
            jQuery('#div-chat').animate({scrollTop: jQuery('#' + id).prop("scrollHeight")}, 350); // Movimiento de scroll
            if (ocultar == false) {
                setTimeout(
                    function() {
                        jQuery("#m").prop('disabled', false);
                        jQuery("#m").focus();
                    }, 300);
            }
        }, 400);
}
jQuery(document).ready(function() {
    jQuery("#m").keypress(function(e) {
        if (e.which == 13) { // Se ha presionado el botón Enter
            enviar();
        }
    });
    enviar("hola", true, true);

    localStorage.setItem('aleatorio_avatar', Math.round(Math.random() * (1 - 0) + 0));
});
</script>