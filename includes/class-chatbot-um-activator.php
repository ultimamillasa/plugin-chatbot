<?php

/**
 * Fired during plugin activation
 *
 * @link       http://ultimamilla.com.ar
 * @since      1.0.0
 *
 * @package    Chatbot_Um
 * @subpackage Chatbot_Um/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Chatbot_Um
 * @subpackage Chatbot_Um/includes
 * @author     Ultima Milla S.A. <lmorales@ultimamillasa.com.ar>
 */
class Chatbot_Um_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
