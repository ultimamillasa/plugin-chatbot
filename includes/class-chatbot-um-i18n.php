<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://ultimamilla.com.ar
 * @since      1.0.0
 *
 * @package    Chatbot_Um
 * @subpackage Chatbot_Um/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Chatbot_Um
 * @subpackage Chatbot_Um/includes
 * @author     Ultima Milla S.A. <lmorales@ultimamillasa.com.ar>
 */
class Chatbot_Um_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'chatbot-um',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
