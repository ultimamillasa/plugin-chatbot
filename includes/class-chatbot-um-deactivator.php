<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://ultimamilla.com.ar
 * @since      1.0.0
 *
 * @package    Chatbot_Um
 * @subpackage Chatbot_Um/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Chatbot_Um
 * @subpackage Chatbot_Um/includes
 * @author     Ultima Milla S.A. <lmorales@ultimamillasa.com.ar>
 */
class Chatbot_Um_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
