function mostrar_chatbot() {
    jQuery("#btn_flotante_umbot").hide();
    jQuery("#contenedor_umbot").show();
    jQuery("#contenedor_chat").animate({ height: "403px" }, 250, function() {
        jQuery("#m").focus();
    });
}

function ocultar_chatbot() {
    jQuery("#contenedor_chat").animate({ height: "0px" }, 250, function() {
        jQuery("#contenedor_umbot").hide();
        jQuery("#btn_flotante_umbot").show();
    });
}

function limpiar_cadena(cadena) {
    var letras = {
        'á': 'a', 'À': 'A', 'Á': 'A', 'à': 'a',
        'è': 'e', 'È': 'E', 'É': 'E', 'é': 'e',
        'Í': 'I', 'í': 'i', 'Ì': 'I', 'ì': 'i',
        'ó': 'o', 'Ò': 'O', 'Ó': 'O', 'ò': 'o',
        'Ú': 'U', 'ù': 'u', 'Ù': 'U', 'ú': 'u',
        'ñ': 'n', 'Ñ': 'N'
    }
    var expresion = /[áàéèíìóòúùñ]/ig;
    return cadena.replace(expresion, function(e) { return letras[e] });
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function fecha() {
    var date = new Date();
    var hora = addZero(date.getHours());
    var minuto = addZero(date.getMinutes());
    return hora + ":" + minuto;
}

function enviarMensaje(mensaje = "", control = false, ocultar = false) {
    // var mostrar = false; // BORRAR
    if (control == true) {
        mensaje = jQuery.trim(mensaje);
    } else {
        mensaje = jQuery.trim(jQuery("#m").val());
    }
    if (mensaje != "") {
        jQuery("#m").val("");
        if (ocultar == false) {
            mostrar_mensaje("chat-list", mensaje, 1, 0, true);
        }
        mensaje = limpiar_cadena(mensaje);
        mostrar_mensaje("chat-list", "<div align='left' style='height: 20px;'><img src='"+options.chatbot_um_escribiendo_gif+"' width = '20px;'  /></div>", 0, 1, true);
        if (localStorage.getItem('idUsuario') != "") {
            jQuery.ajax({
                method: 'GET',
                url: options.chatbot_um_url_nlp,
                async: true,
                data: {
                    "q": mensaje,
                },
                success: function(respuesta) {
                    respuesta = JSON.parse(respuesta);
                    cargarRespuestas(respuesta);
                },
                error: function(error) {
                    console.log("ERROR:");
                    console.log("Hubo un problema en la comunicación ajax (" + error + ")");
                },
            });
        }
    }
}

function cargarRespuestas(respuestas) {
    if (!jQuery.isEmptyObject(respuestas)) {
        jQuery.each(respuestas, function(a, respuesta) {
            // Respuesta relacionada a texto
            var leyenda = "";
            if (!jQuery.isEmptyObject(respuesta)) {
                leyenda = respuesta.text;
            } else {
                leyenda = options.chatbot_um_mensaje_error;
            }
            setTimeout(
                function() {
                    jQuery("#mensaje_temporal").remove();
                    // Respuesta relacionada a botones
                    var botones = cargarRespuestasConBotones(respuesta.buttons);
                    mostrar_mensaje("chat-list", leyenda + botones, 0, 0);
                }, 1000);
        });
    } else {
        console.log("ERROR:");
        console.log("No se han recibido respuestas desde Umbot");
    }
}

function cargarRespuestasConBotones(botones) {
    var respuesta = "";
    if (botones !== undefined) {
        if (botones.length > 0) {
            jQuery.each(botones, function(b, boton) {
                var cargaUtil = JSON.parse(boton.payload);
                cargaUtil = cargaUtil.valores[0];
                // Parámetros variables del botón
                var href = "";
                var onclick = "";
                switch (cargaUtil.tipo) {
                    case "0":
                        onclick = 'onclick="enviarMensaje(\'' + cargaUtil.texto + '\', true, false)" ';
                        break;
                    case "1":
                        href = 'href="' + cargaUtil.texto + '" target="_blank" ';
                        break;
                }
                // Parámetros fijos del botón
                const clase = 'class="btn_rasa sombra_leve btn" ';
                const estilo = 'style="background: #2279d7; color: #fff; cursor: pointer" ';
                // Contrucción del botón
                respuesta += '<a ' + href + onclick + clase + estilo + '>' + boton.title + '</a>'
            });
            respuesta = "<br />" + respuesta;
        }
    }
    return respuesta;
}

function mostrar_mensaje(id, mensaje, tipo, temporal, ocultar = false) {
    jQuery("#m").prop('disabled', true);
    var posicion = "left";
    var nombre = options.chatbot_um_nombre;
    var avatar = options.chatbot_um_logo_url;
    var clase = "him";
    if (tipo == 1) {
        posicion = "right";
        nombre = "Usuario";
        clase = "me";
    }
    var contenido = "";
    if (temporal == 1) {
        contenido += '<li class="' + posicion + ' clearfix" id="mensaje_temporal">';
    } else {
        contenido += '<li class="' + posicion + ' clearfix">';
    }
    contenido += '<div class="chat-body clearfix">';
    contenido += '<div class="header">';
    switch (posicion) {
        case "right":
            contenido += '<small class="text-muted">';
            contenido += '<span class="icono-arg-reloj"></span> ' + fecha();
            contenido += '</small>';
            contenido += '<strong class="pull-' + posicion + ' primary-font">' + nombre + '</strong>';
            break;
        case "left":
            contenido += '<strong class="primary-font">' + nombre + '</strong>';
            contenido += '<small class="pull-right text-muted">';
            contenido += '<span class="icono-arg-reloj"></span> ' + fecha();
            contenido += '</small>';
            break;
    }
    contenido += '</div>';
    contenido += '<p>';
    contenido += '<div class="sombra ' + clase + '">';
    contenido += mensaje;
    contenido += '</div>';
    contenido += '</p>';
    contenido += '</div>';
    contenido += '</li>';
    jQuery("#" + id).append(contenido);
    setTimeout(
        function() {
            jQuery('#div-chat').animate({ scrollTop: jQuery('#' + id).prop("scrollHeight") }, 350); // Movimiento de scroll
            if (ocultar == false) {
                setTimeout(
                    function() {
                        jQuery("#m").prop('disabled', false);
                        jQuery("#m").focus();
                    }, 300);
            }
        }, 400);
}

jQuery(document).ready(function() {
    jQuery("#m").keypress(function(e) {
        if (e.which == 13) { // Se ejecuta cuando se ha presionado 'enter'
            enviarMensaje();
        }
    });
    setTimeout( // Se usa retardo ya que la ejecución es muy rápida
        function() {
            var identificacion = Math.floor(Math.random() * 999999);
            jQuery.ajax({
                url: "https://api.ipify.org",
                async: false,
                success: function(respuesta) {
                    identificacion = respuesta + "_" + identificacion;
                },
                error: function() {
                    console.log("No se ha podido obtener la información de IP");
                }
            });
            localStorage.setItem('idUsuario', identificacion);
            enviarMensaje("Hola", true, true);
        }, 800);
});