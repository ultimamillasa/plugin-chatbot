<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://ultimamilla.com.ar
 * @since      1.0.0
 *
 * @package    Chatbot_Um
 * @subpackage Chatbot_Um/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
