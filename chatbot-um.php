<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://ultimamilla.com.ar
 * @since             1.0.0
 * @package           Chatbot_Um
 *
 * @wordpress-plugin
 * Plugin Name:       Chatbot UM
 * Plugin URI:        http://umbot.com.ar
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Ultima Milla S.A.
 * Author URI:        http://ultimamilla.com.ar
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       chatbot-um
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CHATBOT_UM_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-chatbot-um-activator.php
 */
function activate_chatbot_um() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-chatbot-um-activator.php';
	Chatbot_Um_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-chatbot-um-deactivator.php
 */
function deactivate_chatbot_um() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-chatbot-um-deactivator.php';
	Chatbot_Um_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_chatbot_um' );
register_deactivation_hook( __FILE__, 'deactivate_chatbot_um' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-chatbot-um.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_chatbot_um() {

	$plugin = new Chatbot_Um();
	$plugin->run();

}
run_chatbot_um();

add_action( 'admin_menu', 'chatbot_um_add_admin_menu' );
add_action( 'admin_init', 'chatbot_um_settings_init' );
add_action('wp_enqueue_scripts', 'Chatbot_widget_enqueue_script');
add_action('wp_enqueue_scripts', 'Chatbot_widget_enqueue_style'); 
add_action( 'wp', 'add_content_to_home' );
add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'salcode_add_plugin_page_settings_link');

function Chatbot_widget_enqueue_script() {   
	switch_to_blog( 1 );
	$options = get_option( 'chatbot-um_settings' );
	restore_current_blog();
	wp_enqueue_script( 'funciones', plugin_dir_url( __FILE__ ) . 'js/funciones.js' );
	wp_localize_script( 'funciones', 'options', $options );

	//wp_enqueue_script('funciones'); // Enqueue it!

}
function Chatbot_widget_enqueue_style() {   
	$options = get_option( 'chatbot-um_settings' );
	$template = $options['chatbot_um_template'];
	wp_register_style($template, plugin_dir_url( __FILE__ ) . 'css/'.$template.'.css', array(), '1.0', 'all');
    wp_enqueue_style($template); // Enqueue it!

    wp_register_style('fa', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',array(), '1.0', 'all');
    wp_enqueue_style('fa'); // Enqueue it!
}

function chatbot_um_add_admin_menu(  ) { 
	add_menu_page( 'Chatbot UM', 'Chatbot UM', 'manage_options', 'chatbot_um', 'chatbot_um_options_page' );
}
add_action('wp_footer', 'insert_chatbot');
function insert_chatbot(){
	switch_to_blog( 1 );
	$options = get_option( 'chatbot-um_settings' );
	restore_current_blog();
	if($options['chatbot_um_titulo_ventana'] == ''){
		$options['chatbot_um_titulo_ventana'] = 'Municipalidad de Guaymallén';
	}
	
	if($options['chatbot_um_logo_url'] == ''){
		$options['chatbot_um_logo_url'] = 'http://www.guaymallen.gob.ar/wp-content/uploads/2019/04/favicon.png';
	}
	if(is_multisite()){
		if($options['chatbot_um_multisite'] != get_current_blog_id()){
			//return false;
		}
	}
	?>
	<pre style="display:none;">
		<?php print_r($options) ?>
	</pre>

	<?php 
	include_once($options['chatbot_um_template'].'.php');
}


function chatbot_um_settings_init(  ) { 

	register_setting( 'pluginPages', 'chatbot-um_settings' );

	add_settings_section(
		'chatbot-um_pluginPages_section', 
		__( 'Configuración', 'chatbot-um' ), 
		'chatbot_um_settings_section_callback', 
		'pluginPages'
	);

	add_settings_field( 
		'chatbot_um_url_nlp', 
		__( 'URL NLP', 'chatbot-um' ), 
		'chatbot_um_text_field_0_render', 
		'pluginPages', 
		'chatbot-um_pluginPages_section' 
	);

	add_settings_field( 
		'chatbot_um_logo_url', 
		__( 'Logo URL', 'chatbot-um' ), 
		'chatbot_um_text_field_1_render', 
		'pluginPages', 
		'chatbot-um_pluginPages_section' 
	);

	add_settings_field( 
		'chatbot_um_escribiendo_gif', 
		__( 'Gif Escribiendo', 'chatbot-um' ), 
		'chatbot_um_text_field_2_render', 
		'pluginPages', 
		'chatbot-um_pluginPages_section' 
	);

	add_settings_field( 
		'chatbot_um_nombre', 
		__( 'Nombre', 'chatbot-um' ), 
		'chatbot_um_text_field_3_render', 
		'pluginPages', 
		'chatbot-um_pluginPages_section' 
	);

	add_settings_field( 
		'chatbot_um_mensaje_error', 
		__( 'Mensaje de salida', 'chatbot-um' ), 
		'chatbot_um_text_field_4_render', 
		'pluginPages', 
		'chatbot-um_pluginPages_section' 
	);

	add_settings_field( 
		'chatbot_um_titulo_ventana', 
		__( 'Titulo de Ventana', 'chatbot-um' ), 
		'chatbot_um_text_field_5_render', 
		'pluginPages', 
		'chatbot-um_pluginPages_section' 
	);
	add_settings_field( 
		'chatbot_um_template', 
		__( 'Template', 'chatbot-um' ), 
		'chatbot_um_text_field_7_render', 
		'pluginPages', 
		'chatbot-um_pluginPages_section' 
	);
	if(is_multisite()){
		add_settings_field( 
			'chatbot_um_multisite', 
			__( 'Multisitios', 'chatbot-um' ), 
			'chatbot_um_text_field_6_render', 
			'pluginPages', 
			'chatbot-um_pluginPages_section' 
		);
	}
	


}


function chatbot_um_text_field_0_render(  ) { 

	$options = get_option( 'chatbot-um_settings' );
	?>
	<input type='text' name='chatbot-um_settings[chatbot_um_url_nlp]' value='<?php echo $options['chatbot_um_url_nlp']; ?>'>
	<?php

}


function chatbot_um_text_field_1_render(  ) { 

	$options = get_option( 'chatbot-um_settings' );
	?>
	<input type='text' name='chatbot-um_settings[chatbot_um_logo_url]' value='<?php echo $options['chatbot_um_logo_url']; ?>'>
	<?php

}

function chatbot_um_text_field_2_render(  ) { 

	$options = get_option( 'chatbot-um_settings' );
	?>
	<input type='text' name='chatbot-um_settings[chatbot_um_escribiendo_gif]' value='<?php echo $options['chatbot_um_escribiendo_gif']; ?>'>
	<?php

}

function chatbot_um_text_field_3_render(  ) { 

	$options = get_option( 'chatbot-um_settings' );
	?>
	<input type='text' name='chatbot-um_settings[chatbot_um_nombre]' value='<?php echo $options['chatbot_um_nombre']; ?>'>
	<?php

}

function chatbot_um_text_field_4_render(  ) { 

	$options = get_option( 'chatbot-um_settings' );
	?>
	<textarea type='text' name='chatbot-um_settings[chatbot_um_mensaje_error]' ><?php echo $options['chatbot_um_mensaje_error']; ?></textarea>
	<?php

}

function chatbot_um_text_field_5_render(  ) { 

	$options = get_option( 'chatbot-um_settings' );
	?>
	<input type='text' name='chatbot-um_settings[chatbot_um_titulo_ventana]' value='<?php echo $options['chatbot_um_titulo_ventana']; ?>'>
	<?php

}
function chatbot_um_text_field_7_render(  ) { 

	$options = get_option( 'chatbot-um_settings' );
	?>
	<select name='chatbot-um_settings[chatbot_um_template]' value='<?php echo $options['chatbot_um_template']; ?>'>
		<option value="template1">Template 1</option>
		<option value="template2">Template 2</option>
	</select>
	<?php

}

function chatbot_um_text_field_6_render(  ) { 
	$options = get_option( 'chatbot-um_settings' );
	if(is_multisite()){
		$sites = get_sites();
		
	?>
	<select type='text' multiple='multiple' name='chatbot-um_settings[chatbot_um_multisite]' value='<?php echo $options['chatbot_um_multisite']; ?>'>
		<option value="0"> - Mostrar en todos los sitios - </option>
		<?php
			foreach( $sites as $subsite ) {
				$selected = '';
				$subsite_id = get_object_vars($subsite)["blog_id"];
				$subsite_name = get_blog_details($subsite_id)->blogname;
				if($options['chatbot_um_multisite'] == $subsite_id){
					$selected = 'selected';
				}
				echo '<option '.$selected.' value="'.$subsite_id.'">' . $subsite_name . '</option>';
			}
		?>
	</select>
	<?php
	}

}


function chatbot_um_settings_section_callback(  ) { 

	echo __( 'ChatBot UMSA', 'chatbot-um' );

}



function salcode_add_plugin_page_settings_link( $links ) {
	$links[] = '<a href="' . admin_url( 'admin.php?page=chatbot_um' ) . '">' . __('Configuración') . '</a>';
	return $links;
}

function chatbot_um_options_page(  ) { 

	?>
	<form action='options.php' method='post'>

		<h2>Chatbot UM</h2>

		<?php
		settings_fields( 'pluginPages' );
		do_settings_sections( 'pluginPages' );
		submit_button();
		?>

	</form>
	<?php

}

?>